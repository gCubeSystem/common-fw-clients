# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.1.1] - [2023-03-29]


- see  #24727 upgrade version just for removing the gcube-staging dependencies 

